<?php


namespace MiCore\MenuBundle;


use MiCore\MenuBundle\DependencyInjection\CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MiCoreMenuBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
       $container->addCompilerPass(new CompilerPass());
    }
}
