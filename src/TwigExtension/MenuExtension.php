<?php


namespace MiCore\MenuBundle\TwigExtension;


use MiCore\MenuBundle\Menu\MenuService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MenuExtension extends AbstractExtension
{
    /**
     * @var MenuService
     */
    private $menuService;

    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    protected function functions(): array
    {
        return [
            'menu' => function ($tags = []) {
            if (!is_string($tags) && !is_array($tags) && !is_null($tags)){
                throw new \InvalidArgumentException('Expected string, array or Null!');
            }

                return $this->menuService->createMenu(is_string($tags) ? [$tags] : $tags);
            }
        ];
    }

    /**
     * @return array|TwigFunction[]
     */
    final public function getFunctions()
    {
        foreach ($this->functions() as $name => $filter){
            $result[] = new TwigFunction($name, $filter);
        }
        return $result ?? [];
    }

}
