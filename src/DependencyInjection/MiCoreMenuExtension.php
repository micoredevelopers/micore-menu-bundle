<?php


namespace MiCore\MenuBundle\DependencyInjection;



use Doctrine\Common\Annotations\AnnotationReader;
use MiCore\MenuBundle\Menu\Loader\AnnotationMenuLoader;
use MiCore\MenuBundle\Menu\Loader\MenuLoaderInterface;
use MiCore\MenuBundle\Menu\MenuService;
use MiCore\MenuBundle\TwigExtension\MenuExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class MiCoreMenuExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $container->autowire(AnnotationReader::class);
        $container->autowire(AnnotationMenuLoader::class)->setArguments([
            '$namespace' => 'App\\Controller',
            '$dir' => '%kernel.project_dir%/src/Controller'
        ]);


        $container
            ->registerForAutoconfiguration(MenuLoaderInterface::class)
            ->addTag(MenuLoaderInterface::TAG_NAME);

        $container
            ->autowire(MenuService::class)
            ->addMethodCall('loadMenus', [new Reference(AnnotationMenuLoader::class)])
        ;

        $container
            ->autowire(MenuExtension::class)
            ->addTag('twig.extension');

        if (isset($_ENV['APP_ENV']) && 'test' == $_ENV['APP_ENV']){
            foreach ($container->getDefinitions() as $definition){
                $definition->setPublic(true);
            }
        }
    }
}
