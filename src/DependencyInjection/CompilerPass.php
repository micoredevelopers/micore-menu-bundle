<?php


namespace MiCore\MenuBundle\DependencyInjection;


use MiCore\MenuBundle\Menu\Loader\MenuLoaderInterface;
use MiCore\MenuBundle\Menu\MenuService;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {

       $menuServiceDefinition = $container->getDefinition(MenuService::class);
      foreach ($container->findTaggedServiceIds(MenuLoaderInterface::TAG_NAME) as $id=>$tags){
           $loaderDefinition = $container->getDefinition($id);
           $menuServiceDefinition->addMethodCall('loadMenus', [$loaderDefinition]);
       }
    }
}
