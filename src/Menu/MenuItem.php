<?php


namespace MiCore\MenuBundle\Menu;

/**
 * Class MenuItem
 * @package MiCore\MenuBundle\Menu
 * @property $icon
 * @property $name
 * @property $route
 * @property $route_params
 */
class MenuItem
{

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var string
     */
    private $id;

    /**
     * @var string|null
     */
    private $parent;

    /**
     * @var array
     */
    private $tags = [];

    /**
     * @var integer
     */
    private $sort = 0;

    public function __construct(string $id, $parent = null)
    {
        $this->id = $id;
        $this->parent = $parent;
    }

    public function __get($name)
    {
        return $this->getParam($name);
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return MenuItem
     */
    public function setParams(array $params): MenuItem
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getParam(string $name)
    {
        return $this->params[$name] ?? null;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return MenuItem
     */
    public function setId(?string $id): MenuItem
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getParent(): ?string
    {
        return $this->parent;
    }

    /**
     * @param string|null $parent
     * @return MenuItem
     */
    public function setParent(?string $parent): MenuItem
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return MenuItem
     */
    public function setTags(array $tags): MenuItem
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @param string $tag
     * @return bool
     */
    public function tagExist(string $tag): bool
    {
        return in_array($tag, $this->tags);
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return $this
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

}
