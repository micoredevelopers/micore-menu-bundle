<?php


namespace MiCore\MenuBundle\Menu;


/**
 * Class MenuViewItem
 * @package MiCore\MenuBundle\Menu
 * @property $icon
 * @property $name
 * @property $route
 * @property $route_params
 */
class MenuViewItem
{

    /**
     * @var MenuItem
     */
    private $menuItem;

    /**
     * @var MenuItem[]
     */
    private $children = [];

    public function __construct(MenuItem $menuItem)
    {
        $this->menuItem = $menuItem;
    }

    public function __get($name)
    {
      return $this->menuItem->getParam($name);
    }

    public function __call($name, $arguments)
    {
        return $this->menuItem->getParam($name);
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->menuItem->getParams();
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getParam(string $name)
    {
        return $this->menuItem->getParam($name);
    }

    /**
     * @param MenuItem $menuItem
     * @return $this
     */
    public function addMenuItem(MenuItem $menuItem): self
    {
        $this->children[] = $menuItem;
        return $this;
    }

    /**
     * @return MenuItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

}
