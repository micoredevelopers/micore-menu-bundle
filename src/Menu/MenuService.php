<?php


namespace MiCore\MenuBundle\Menu;


use MiCore\MenuBundle\Menu\Loader\MenuLoaderInterface;

class MenuService
{

    /**
     * @var MenuItem[]
     */
    private $menus = [];

    /**
     * @param MenuItem $menuItem
     * @return $this
     */
    public function addMenu(MenuItem $menuItem): self
    {
        $this->menus[$menuItem->getId()] = $menuItem;
        return $this;
    }

    /**
     * @param MenuLoaderInterface $menuLoader
     * @return $this
     */
    public function loadMenus(MenuLoaderInterface $menuLoader): self
    {
        $menuLoader->load($this);
        $this->sortMenu();
        return $this;
    }

    /**
     * @param array $tags
     * @return MenuViewItem[]
     */
    public function createMenu(array $tags = []): array
    {
         foreach ($this->menus as $menu){
             if ($tags && !array_intersect($menu->getTags(), $tags)){
                 continue;
             }
             if (null === $menu->getParent()){

                 $result[$menu->getId()] = new MenuViewItem($menu);
                 continue;
             }

             if (!isset($this->menus[$menu->getParent()])){
                 $result[$menu->getParent()] = new MenuViewItem(new MenuItem($menu->getParent()));
             } else if (!isset($result[$menu->getParent()])){
                 $result[$menu->getParent()] = new MenuViewItem($this->menus[$menu->getParent()]);
             }

             $result[$menu->getParent()]->addMenuItem($menu);
         }
         return $result ?? [];
    }

    public function sortMenu()
    {
        uasort($this->menus, function (MenuItem $menuItemA, MenuItem $menuItemB){
            if ($menuItemA->getSort() === $menuItemB->getSort()){
                return 0;
            }
            return $menuItemA->getSort() > $menuItemB->getSort() ? 1 : -1;
        });
    }

}
