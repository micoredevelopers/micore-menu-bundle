<?php


namespace MiCore\MenuBundle\Menu\Loader;


use Throwable;

class MenuIdResolverException extends \Exception
{

    public function __construct($code = 0, Throwable $previous = null)
    {
        parent::__construct('No Route Name', $code, $previous);
    }

}
