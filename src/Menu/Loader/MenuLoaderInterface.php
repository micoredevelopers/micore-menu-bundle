<?php


namespace MiCore\MenuBundle\Menu\Loader;


use MiCore\MenuBundle\Menu\MenuService;

interface MenuLoaderInterface
{

    const TAG_NAME = 'micore.menu_bundle.menu_loader';

    /**
     * @param MenuService $menuService
     */
    public function load(MenuService $menuService): void;
}
