<?php


namespace MiCore\MenuBundle\Menu\Loader;


/**
 * Class Menu
 * @package Advance\SymfonyBundle\DependencyInjection
 * @property $icon
 * @property $name
 * @property $route
 * @property $routeParams
 * @Annotation
 */
class Menu
{
    /**
     * @var array
     */
    public $params = [];

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $parent;

    /**
     * @var array
     */
    public $tags = [];

    /**
     * @var integer
     */
    public $sort = 0;

    public function __construct($v)
    {

        foreach ($v as $k=>$item){
            if ('value' === $k){
                $this->id = $item;
            } else if ('params' === $k){
                $this->params = array_merge($this->params ?? [], $item);
            } else if (property_exists($this, $k)){
                $this->$k = $item;
            } else {
                $this->params[$k] = $item;
            }
        }
    }
}
