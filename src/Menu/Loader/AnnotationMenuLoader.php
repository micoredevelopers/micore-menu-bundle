<?php


namespace MiCore\MenuBundle\Menu\Loader;

use MiCore\MenuBundle\Menu\MenuItem;
use MiCore\MenuBundle\Menu\MenuService;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Routing\Annotation\Route;

class AnnotationMenuLoader implements MenuLoaderInterface
{

    private $dir;
    /**
     * @var AnnotationReader
     */
    private $annotationReader;
    /**
     * @var string
     */
    private $namespace;

    public function __construct($dir, AnnotationReader $annotationReader, $namespace = 'App\\Controller')
    {
        $this->dir = $dir;
        $this->annotationReader = $annotationReader;
        $this->namespace = $namespace;
    }

    /**
     * @param MenuService $menuService
     * @throws MenuIdResolverException
     * @throws \ReflectionException
     */
    public function load(MenuService $menuService): void
    {

        $annotations = [];
        $this->readAnnotations($this->dir, $this->namespace, $annotations);


        foreach ($annotations as $annotation) {
            /**
             * @var Menu $menu
             * @var Route $route
             */
            $menus = $annotation();
            $route = $annotation(Route::class);
            $routeName = $route ? $route->getName() : null;

            foreach ($menus as $menu) {
                if (!$menu instanceof Menu) {
                    continue;
                }
                $id = $menu->id;

                if (!$id){
                    $id = $menu->params['route'] ?? $routeName ?? null;
                }

                if (!$id){
                    throw new MenuIdResolverException();
                }

                $menu->params['route'] = $menu->params['route'] ?? $id;
                $menu->params['name'] = $menu->params['name'] ?? $id;

                $menuItem = new MenuItem($id, $menu->parent);
                $menuItem
                    ->setParams($menu->params)
                    ->setSort($menu->sort)
                    ->setTags($menu->tags);
                $menuService->addMenu($menuItem);
            }
        }
    }

    /**
     * @param string $dir
     * @param string $namespace
     * @param $annotations
     * @throws \ReflectionException
     */
    private function readAnnotations(string $dir, string $namespace, &$annotations): void
    {
        foreach (scandir($dir) as $file) {
            if (in_array($file, ['.', '..'])){
                continue;
            }
            $path = $dir . '/' . $file;
            $nSpace = str_replace('.php', '', $namespace . '\\' . $file);
            if (is_dir($path)) {
                $this->readAnnotations($path, $nSpace, $annotations);
                continue;
            }
            if (!is_file($path) || !class_exists($nSpace)) {
                continue;
            }

            $reflectionClass = new \ReflectionClass($nSpace);
            $annotations[] = function (string $annotationName = null) use ($reflectionClass) {
                if ($annotationName) {
                    return $this->annotationReader->getClassAnnotation($reflectionClass, $annotationName);
                }
                return  $this->annotationReader->getClassAnnotations($reflectionClass);
            };

            foreach ($reflectionClass->getMethods() as $method) {
                if ($method->class !== $nSpace){
                    continue;
                }
                $annotations[] = function (string $annotationName = null) use ($method) {
                    if ($annotationName) {
                        return $this->annotationReader->getMethodAnnotation($method, $annotationName);
                    }
                    return $this->annotationReader->getmethodAnnotations($method);
                };
            }

        }
    }
}
