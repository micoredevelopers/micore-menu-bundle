<?php


namespace MiCore\MenuBundle\Tests;


use MiCore\MenuBundle\Menu\MenuService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MenuTest extends KernelTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testMenu()
    {
        /**
         * @var MenuService $menuService
         */
        $menuService = self::$container->get(MenuService::class);
        $this->assertEquals($menuService->createMenu(['admin'])['main']->name, 'main');
        $this->assertEquals($menuService->createMenu(['admin'])['main']->getChildren()[0]->route, 'sub');
        $this->assertEquals($menuService->createMenu(['public'])['product']->getParam('enable'), 1);

        $this->assertEquals(count($menuService->createMenu(['admin', 'public'])), count($menuService->createMenu(['all'])));

    }

    public function testTwigExtension()
    {
        $twig = self::$container->get('twig');
        $render = $twig->render('test.twig');
        $this->assertEquals($render, 'mainmaincategoryCategoriesproductProducts');
    }

}
