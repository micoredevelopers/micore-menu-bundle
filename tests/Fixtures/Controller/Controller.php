<?php


namespace MiCore\MenuBundle\Tests\Fixtures\Controller;

use MiCore\MenuBundle\Menu\Loader\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestController
 * @package MiCore\MenuBundle\Tests\Fixtures\Controller
 *
 * @Menu(name="Products", route="product", params={"enable": 1}, tags={"public", "all"}, sort=2)
 * @Menu(name="Categories", route="category", tags={"public", "all"}, sort=1)
 */
class Controller extends AbstractController
{

    /**
     * @Route("/products-list", name="products.list")
     * @Menu(parent="product", tags={"public", "all"})
     */
    public function productsList()
    {

    }

    /**
     * @Route("/products-list", name="products.list")
     * @Menu(parent="category", tags={"public", "all"})
     */
    public function categoriesList()
    {

    }

}
