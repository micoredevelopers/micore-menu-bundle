<?php


namespace MiCore\MenuBundle\Tests\Fixtures\Controller\Admin;


use MiCore\MenuBundle\Menu\Loader\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class AdminController
 * @package MiCore\MenuBundle\Tests\Fixtures\Controller\Admin
 *
 * @Menu(route="main", tags={"admin", "all"}, sort=0)
 */
class AdminController extends AbstractController
{

    /**
     * @Menu(route="sub", parent="main", tags={"admin", "all"})
     */
    public function index(){}

}
