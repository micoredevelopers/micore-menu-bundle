<?php

namespace MiCore\MenuBundle\Tests\Fixtures;

use MiCore\KernelTest\KernelTest;
use MiCore\MenuBundle\Menu\Loader\AnnotationMenuLoader;
use MiCore\MenuBundle\MiCoreMenuBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Kernel extends KernelTest
{
    protected function bundles(): array
    {
        return [
            MiCoreMenuBundle::class,
            TwigBundle::class
        ];
    }

    public function configureContainer(ContainerBuilder $c)
    {
        $c->autowire(AnnotationMenuLoader::class)->setArguments([
            '$dir' => __DIR__ . '/Controller',
            '$namespace' => 'MiCore\MenuBundle\Tests\Fixtures\Controller'
        ]);
        $c->loadFromExtension('twig', [
            'paths' => [__DIR__.'/Resource']
        ]);
    }
}

