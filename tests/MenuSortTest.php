<?php


namespace MiCore\MenuBundle\Tests;


use MiCore\MenuBundle\Menu\MenuService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class MenuSortTest extends KernelTestCase
{
    protected function setUp(): void
    {
        self::bootKernel();
    }

    public function testSort()
    {
        $menuService = self::$container->get(MenuService::class);
        $menu = $menuService->createMenu(['all']);
        $menuArr = array_values($menu);


        $this->assertEquals('main', $menuArr[0]->route);
        $this->assertEquals('category', $menuArr[1]->route);
        $this->assertEquals('product', $menuArr[2]->route);

    }

}
